# Monitoring NEXTGEN Connect (Mirth Connect) using PRTG

With this powershell script you are able to monitor your [NEXTGEN Connect](https://www.nextgen.com/products-and-services/integration-engine) installation. 

Since version 4.3 Mirth has a REST API which is queried by the script. This allows two types of sensors to be created in PRTG. The Mirth System Health Sensor provides system values such as CPU load, free memory, and free hard disk space. To monitor individual interfaces, the Channel Sensor monitors the number of messages sent and received as well as faulty and buffered messages per channel. With these sensors, it is now possible to monitor important parameters of the communication server and thus to integrate a central and important part of the medical infrastructure in PRTG. The sensors can also be created with a template, which simplifies the integration. By regularly performing Auto-Discovery, Mirth's newly created channels can be integrated directly into PRTG.

### Download
Please use this link to download [NEXTGen Connect(Mirth) Install package](https://gitlab.com/PRTG/Sensor-Scripts/eHealth-NEXTGEN-Connect/-/jobs/artifacts/master/download?job=PRTGDistZip)


### Installation
The sensor project has a standard directory structure:
All the files in the PRTG subdirectory needs to go into the PRTG program directory
[How and where does PRTG store its data](https://kb.paessler.com/en/topic/463-how-and-where-does-prtg-store-its-data).
The other files are for documentation and testing.

<div class="panel panel-info">
Directory Layout:

<div class="panel-body">
<pre>
 ProjectName
   + - traces		         (files to use for testing, SNMP traces etc.)
   + - PRTG                  (hierarchy that goes into the PRTG directory)
        + - Custom Sensors	 (Where PRTG stores Custom senosrs)
        + - devicetemplates  (Where PRTG stores the device templates)
        + - lookups          (Where PRTG stores the lookups)
            + - custom       (Where PRTG stores custom lookups)
        + - MIB              (Where PRTG stores MIBs)
        + - notifications	 (Where PRTG stores Custom Notifications)
        + - snmplibs         (Where PRTG stores imported custom OID Libraries)
        + - webroot          (PRTG webgui)
            + - icons        (PRTG webgui icons)
                + - devices  (PRTG device icons)
</pre>
</div>
</div>

The download package listed above has all the files in the correct structure to unzip directly into the PRTG Directory.

Following metrics can be monitored:
 
#### Mirth System Health:
- CPU %
- Free Memory
- Free Disk Space

```
prtg_mirth.ps1 <IP/DNS> <port> <username> <password> "system" 
```

![Image of Mirth System Health Sensor](./Images/mirth_system_health.png)

#### Channel <Channel Name>:
- Received
- Sent 
- Filtered
- Error
- Queued

```
prtg_mirth.ps1 <IP/DNS> <port> <username> <password> "channel" <channelID> 
```
![Image of Mirth Channel Sensor](./Images/mirth_channel.png)

# Auto-Discovery support via Template
  - Select "NEXTGEN (Mirth) Connect" template in Device Settings as template
  - Set Username and Password in the Device Linux credentials
  - Run Auto-Discovery with specified template
  - Newly added channels from Mirth will be added in PRTG if you schedule a reoccuring Auto-Discovery for this Device

# Debugging
- Add -Verbose parameter to enable logging to console

## Mirrored from:
https://github.com/0xLigety/PRTG-Healthcare/tree/master/NEXTGEN%20(Mirth)%20Connect



### Included Files

#### Custom Sensors
PRTG Custom Sensor files.

#### devicetemplates (xxx.odt) 
PRTG device template files.

#### lookups\custom (xxx.ovl) 
PRTG lookup files for use with sensors with lookup values

#### notifications
Custom PRTG Notifications.

#### MIB
Manufacturer supplied MIB files to include. These will be loaded into PRTG Core server on startup.

#### snmplibs (xxx.oidlib)
Files imported from MIB files by the [Paessler MIB importer](https://www.paessler.com/tools/mibimporter).

#### webroot\icons\devices (vendors_xxx.png)
Icon files used by PRTG to display in the tree. 
 -PNG's: Should be 14.14 pixels.
 -SVG's; Should be small images for best presentation 



